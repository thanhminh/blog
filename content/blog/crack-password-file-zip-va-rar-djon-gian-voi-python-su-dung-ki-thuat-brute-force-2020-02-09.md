---
title: Crack Password File Zip và Rar đơn giản với Python sử dụng kĩ thuật Brute Force
tags: python, brute force, cracking
category: Hacking
excerpt: Các thư viện cần dùng
created: 2018-03-08
image: ./images/crack-password-file-zip-va-rar-djon-gian-voi-python-su-dung-ki-thuat-brute-force-2020-02-09.jpg
image_caption: Crack Password File Zip và Rar đơn giản với Python sử dụng kĩ thuật Brute Force
author: mnismt
---

# 1. Các thư viện cần dùng

Thư viện [zipfile](https://docs.python.org/3/library/zipfile.html) để xử lí file Zip.

Thư viện [rarfile](http://rarfile.readthedocs.io/en/latest/api.html) để xử lí file Rar.

Thư viện [UnRAR](http://www.rarlab.com/rar/unrarw32.exe), thư viện rarfile bắt buộc đi kèm với thư viện này

Thư viện [os](https://docs.python.org/3/library/os.html) để kiểm tra file.

Thư viện [argparse](https://docs.python.org/3/library/argparse.html)  để nhận các Argument.

Thư viện [threading](https://docs.python.org/3/library/threading.html) để tăng tốc bằng cách xử lí đa luồng.

Thư viện [itertools](https://docs.python.org/3/library/itertools.html) để tạo các chuỗi password.

Thư viện [time](https://docs.python.org/3/library/time.html) để tính thời gian (cái này mình đưa vào để tính xem mất bao lâu thôi chứ nó không bắt buộc ).

# 2. Tiến hành

Đầu tiên ta tạo một biến lưu giá trị của tất cả các kí tự trên bàn phím:

```
CHARACTER ='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&()'

```

 Sau đó viết các argument cần thiết:
 
 ```
parser = argparse.ArgumentParser(description='Crack zip rar', epilog='Use the -h for help')
parser.add_argument('-i','--input', help='Insert the file path of compressed file', required=True)
parser.add_argument('rules', nargs='*', help='<min> <max> <character>')
 ```
 
Ta viết một class check các argument:

```
class Check:
    def __init__(self, Arg):
        self.type = None
        self.rules = False
        self.minLength = None
        self.maxLength = None
        self.character = None
        # Kiểm tra rules
        if len(Arg) >= 4:
            self.getData(Arg)
            self.rules = True
        elif len(Arg) == 0 or len(Arg) > 2:
            parser.print_help()
            parser.exit()
        # Kiểm tra file có tồn tại hay không
        if (self.CheckFileExist(Arg)):
            self.getType(Arg)
        else:
            print ('No such file or directory: ',Arg[1])
            parser.exit()

    def CheckFileExist(self, Arg):
        if (os.path.isfile(Arg[1])):
            return True
        else:
            return False

    def getData(self, Arg):
        try:
            self.minLength = int(Arg[2])
            self.maxLength = int(Arg[3])
        except ValueError:
            print ('Value Error')
            parser.exit()
        if self.minLength > self.maxLength:
            print ('Length Error')
            parser.exit()
        if len(Arg) == 5:
            self.character = Arg[4]
    
    def getType(self, Arg):
        if os.path.splitext(Arg[1])[1] == ".rar" or os.path.splitext(Arg[1])[1]==".zip":
            self.type = os.path.splitext(Arg[1])[1]
        else:
            print ('Extension Error')
            parser.exit()
```


Tiếp tục ta viết class chính để xử lí:

```
class Handler:
    def __init__(self, rules, typeCompress, minLength, maxLength, character):
        self.rules = rules
        self.location = sys.argv[2]
        self.type = typeCompress
        self.minLength = minLength
        self.maxLength = maxLength
        if not character:
            self.character = CHARACTER
        else:
            self.character = character
        self.result = False

        self.GetFile()
        self.CheckRules()

    def GetFile(self):
        # Khai báo file
        if self.type == '.zip':
            self.FileCrack = zipfile.ZipFile(self.location)
        else:
            self.FileCrack = rarfile.RarFile(self.location)

    def Brute(self,password):
        # Brute với các pass đưa vào, nếu .zip thì phải encode string password thành utf-8 vì module zipfile bắt buộc
        try:
            if self.type == '.zip':
                tryPass = password.encode()
            else:
                tryPass = password
            print (tryPass)
            self.FileCrack.extractall(pwd=tryPass)
            print ('Complete')
            print('Time:',time.clock() - self.start_time,'s')
            print ('Password:',password)
            self.result = True
        except:
            pass

    def CheckRules(self):
        self.start_time = time.clock()
        print ('Cracking...')
        # Nếu không có rules thì lặp vô hạn
        if not self.rules:
            length = 1
            while True:
                self.SendRequest(length)
                if self.result:
                    return
                length += 1
        # Nếu có rules thì sẽ lặp từ độ dài <min> <max> của các argument đưa vào
        else:
            for length in range(self.minLength, self.maxLength + 1):
                self.SendRequest(length)
                if self.result:
                    return
            if not self.result:
                print ('Cannot find password with this rules')
                return

    def SendRequest(self,length):
        listPass = product(self.character, repeat=length)
        for Pass in listPass:
            tryPass = ''.join(Pass)
            # Multi Thread:
            # nThread = Thread(target=self.Brute, args=(tryPass, ))
            # nThread.start()

            # Single Thread: 
            self.Brute(tryPass)
            if self.result:
                return
```

Toàn bộ code:

```
import zipfile
import rarfile
import os
import sys
from threading import Thread
import argparse
from itertools import product
import time

parser = argparse.ArgumentParser(description='Crack zip rar', epilog='Use the -h for help')
parser.add_argument('-i','--input', help='Insert the file path of compressed file', required=True)
parser.add_argument('rules', nargs='*', help='<min> <max> <character>')

CHARACTER = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()'


class Check:
    def __init__(self, Arg):
        self.type = None
        self.rules = False
        self.minLength = None
        self.maxLength = None
        self.character = None
        # Kiểm tra rules
        if len(Arg) >= 4:
            self.getData(Arg)
            self.rules = True
        elif len(Arg) == 0 or len(Arg) > 2:
            parser.print_help()
            parser.exit()
        # Kiểm tra file có tồn tại hay không
        if (self.CheckFileExist(Arg)):
            self.getType(Arg)
        else:
            print ('No such file or directory: ',Arg[1])
            parser.exit()

    def CheckFileExist(self, Arg):
        if (os.path.isfile(Arg[1])):
            return True
        else:
            return False

    def getData(self, Arg):
        try:
            self.minLength = int(Arg[2])
            self.maxLength = int(Arg[3])
        except ValueError:
            print ('Value Error')
            parser.exit()
        if self.minLength > self.maxLength:
            print ('Length Error')
            parser.exit()
        if len(Arg) == 5:
            self.character = Arg[4]
    
    def getType(self, Arg):
        if os.path.splitext(Arg[1])[1] == ".rar" or os.path.splitext(Arg[1])[1]==".zip":
            self.type = os.path.splitext(Arg[1])[1]
        else:
            print ('Extension Error')
            parser.exit()

class Handler:
    def __init__(self, rules, typeCompress, minLength, maxLength, character):
        self.rules = rules
        self.location = sys.argv[2]
        self.type = typeCompress
        self.minLength = minLength
        self.maxLength = maxLength
        if not character:
            self.character = CHARACTER
        else:
            self.character = character
        self.result = False

        self.GetFile()
        self.CheckRules()

    def GetFile(self):
        # Khai báo file
        if self.type == '.zip':
            self.FileCrack = zipfile.ZipFile(self.location)
        else:
            self.FileCrack = rarfile.RarFile(self.location)

    def Brute(self,password):
        # Brute với các pass đưa vào, nếu .zip thì phải encode string password thành utf-8 vì module zipfile bắt buộc
        try:
            if self.type == '.zip':
                tryPass = password.encode()
            else:
                tryPass = password
            print (tryPass)
            self.FileCrack.extractall(pwd=tryPass)
            print ('Complete')
            print('Time:',time.clock() - self.start_time,'s')
            print ('Password:',password)
            self.result = True
        except:
            pass

    def CheckRules(self):
        self.start_time = time.clock()
        print ('Cracking...')
        # Nếu không có rules thì lặp vô hạn
        if not self.rules:
            length = 1
            while True:
                self.SendRequest(length)
                if self.result:
                    return
                length += 1
        # Nếu có rules thì sẽ lặp từ độ dài <min> <max> của các argument đưa vào
        else:
            for length in range(self.minLength, self.maxLength + 1):
                self.SendRequest(length)
                if self.result:
                    return
            if not self.result:
                print ('Cannot find password with this rules')
                return

    def SendRequest(self,length):
        listPass = product(self.character, repeat=length)
        for Pass in listPass:
            tryPass = ''.join(Pass)
            # Multi Thread:
            # nThread = Thread(target=self.Brute, args=(tryPass, ))
            # nThread.start()

            # Single Thread: 
            self.Brute(tryPass)
            if self.result:
                return
def main():
    check = Check(sys.argv[1:])
    args = parser.parse_args()
    rarfile.UNRAR_TOOL = "UnRAR.exe"
    Handling = Handler(check.rules, check.type, check.minLength, check.maxLength, check.character)
if __name__ == '__main__':
    main()
```

# 3. Thử nghiệm

### Không rules

Mình tạo một file zip tên là `Takizawa Laura.zip` với pass là `jav`

![alt text](https://s3-ap-southeast-1.amazonaws.com/kipalog.com/802llpg7tg_1.png)

Chạy thử và kết quả:

![alt text](https://s3-ap-southeast-1.amazonaws.com/kipalog.com/yzxax7vwym_1.png)

Ban đầu, list pass được tạo với length = 1 sẽ có dạng [a, b, c,...] sau đó không được tiếp tục tăng length = 2 nên list trở thành [aa, ab, ac,...] cũng không được nốt, length = 3 thì [aaa, aab,..., **jav**,...].

### Có rules

Mình tạo một file rar tên là `PPPD-558.rar` với pass là `boobs`

![alt text](https://s3-ap-southeast-1.amazonaws.com/kipalog.com/jm0pj0vwa_2.png)

Chạy thử với rules ` 4 5 abos `:

![alt text](https://s3-ap-southeast-1.amazonaws.com/kipalog.com/mjj8814d1n_3.png)

Ban đầu, list pass được tạo với length = 4 **(min Length)** sẽ có dạng [aaaa,aaab,,...] sau đó không được tiếp tục tăng length = 5 **(max Length)** ( nên list trở thành [aaaaa, aaaab, ..., **boobs**,...] .

# 4. Lời kết

Đây là một cách crack file zip đơn giản sử dụng kĩ thuật Brute Force, ưu điểm là luôn tìm ra password, nhưng nhược điểm là phải mất kha khá thời gian với các mật khẩu dài dòng, ví dụ như: **yaxua.laktr0j** hay **racjngp0yvjppr0** chẳng hạn.

Sử dụng thread có thể tăng tốc lên một ít,nhưng các bạn cần limit thread lại kẻo treo máy bằng cách dùng thư viện [queue](https://docs.python.org/3.6/library/queue.html).

Mình cũng đã thêm rules vào để tối ưu hóa khả năng tìm pass, với rules các bạn có thể giảm số lượng pass thử, ví dụ như các bạn đoán pass dài khoảng `6 - 9` kí tự và lượng character chỉ gồm có các kí tự gồm `[x, h, a, m, s, t, e, r]` thì lúc đó sẽ rút gọn được rất nhiều thời gian.

Các bạn có thể fork project của mình trên [github](https://github.com/mnismt/CompressedCrack).
Cảm ơn bạn đã đọc bài. :smile:
