---
title: Kiểm tra kết nối internet bằng Javascript
tags: javascript, internet, TIL
category: TIL
excerpt: Cách kiểm tra xem ta có đang kết nối với internet không trên trình duyệt qua Javascript
created: 2017-10-19
image: ./images/kiem-tra-ket-noi-internet-bang-javascript-2020-02-09.jpg
image_caption: Kiểm tra kết nối internet bằng Javascript
author: mnismt
---

Để kiểm tra xem có đang kết nối với internet không trên Javascript khá là đơn giản, chúng ta có 2 cách:

# navigator.onLine

Đây là cách đơn giản nhất, property `onLine` trong object `navigator` sẽ trả về `true` nếu user đang kết nối với internet và ngược lại

```javascript
if (navigator.onLine) {
  // User đang kết nối với internet
}
```

# Dùng sự kiện

Cách này thì ta thêm vào object window 2 thằng event là "offline" và "online" là xong:

```javascript
window.addEventListener('online',  updateStatus);
window.addEventListener('offline', updateStatus);

function updateStatus() {
  // Callback function này được gọi khi user ngắt hoặc kết nối với internet
}
```
