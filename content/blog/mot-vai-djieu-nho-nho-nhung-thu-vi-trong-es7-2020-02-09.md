---
title: Một vài điều nho nhỏ nhưng thú vị trong ES7 & ES8
tags: Javascript
category: Javascript
excerpt: Một vài điều nho nhỏ nhưng thú vị trong ES7 & ES8
created: 2018-01-25
image: ./images/mot-vai-djieu-nho-nho-nhung-thu-vi-trong-es7-2020-02-09.jpg
image_caption: Một vài điều nho nhỏ nhưng thú vị trong ES7 & ES8
author: mnismt
---

Mỗi năm thì lại có một bản ECMAScript được thêm vào, cùng với đó là thêm kha khá tính năng mới, bài viết này sẽ kể đến một vài tính năng nhỏ nhưng khá là hay trong ES7 và ES8.

# Object.entries
Method này trả về 1 mảng dựa trên property `[key, value]` của Object được đưa vào, chẳng hạn:
```javascript
// String
Object.entries('circle') // [["0","c"],["1","i"],["2","r"],["3","c"],["4","l"],["5","e"]]

// Object
Object.entries({ 'j': 'e', 'r': 'k'}) // [["j","e"],["r","k"]]
```

# Object.values
Method này trả về 1 mảng dựa trên property `value` của Object đó, ví dụ:

```javascript
// String
Object.values('Bareback') // ["B", "a", "r", "e", "b", "a", "c", "k"]

// Array
Object.values([6, 6, 6]) // [6, 6, 6]

// Object
Object.values({ 'q': 23, 'w': 25, 'e': 27 }) // [23, 25, 27]

// Đặc biệt
Object.values({ 1: 'a', 2: 'b', 0: 1 }) // [1, "a", "b"]
```

# String.prototype.padStart & padEnd

2 method này nhận 2 param: `targetLength` và `padString`, nó có nhiệm vụ là thêm string `padString` vào string gốc với độ dài chỉ định là `targetLength`, nó sẽ tiếp tục thêm `padString` nếu như string mới có độ dài <  `targetLength`.
Sự khác nhau của 2 method chỉ khác là `padStart` sẽ thêm vào phía trái, còn `padEnd` thì ở bên phải,  ví dụ:

```javascript
// Thêm string 'hi' vào trước string '' và chỉ định độ dài string trả về là 6.
''.padStart(6, 'hi') // 'hihihi'

// Thêm 'agging' vào sau 'Teab'.
'Teab'.padEnd(10, 'agging') // 'Teabagging'

// Viết như thế này sẽ nhận được 'abcdef' 
// vì 'abcdef' có độ dài bằng 6 và bằng targetLength.

'def'.padStart(6, 'abc') // 'abcdef'

/* Nếu ta thay đổi targetLength = 9 thì ta sẽ nhận string trả về là 'abcabcdef' vì sau khi thêm 'abc' 
vào trước 'def', string mới là 'abcdef' có độ dài = 6 < 9, vì vậy string mới sẽ tiếp tục thêm 'abc' 
vào để có độ dài = targetLength.
*/

'def'.padStart(9, 'abc') // 'abcabcdef'

'qwert'.padStart(10, 'abc') // 'abcabqwert'
```

# Array.prototype.includes
Method này sẽ kiểm tra giá trị được đưa vào có trùng với mảng ban đầu hay không, nếu có thì `return true` và ngược lại.

```javascript
['Rina Ishihara', 'Asuka Ichinose', 'Kirara Asuka'].includes('Kirara Asuka') 
// true

['Ava Taylor', 'Karla Kush', 'Brooklyn Chase'].includes('Johnny Sins') 
// false
```

Nó cũng tương tự như khi ta dùng method `indexOf` để kiểm tra vị trí của element trong Array, nếu bằng -1 thì không tồn tại và ngược lại vậy.

# Lũy thừa

Với các phiên bản trước, để tính lũy thừa thì ta làm thế này:

```javascript
// 6^3
Math.pow(6, 3) // 216
```

Với ES7 & ES8 thì có thể viết gọn thế này:

```javascript
6 ** 3 // 216
```

# Dấu , sau các tham số của hàm

## Định nghĩa thông số (Parameter definitions)

Có dấu phẩy `,` đằng sau sẽ không ảnh hưởng gì đến độ dài của parameter, ví dụ:

```javascript
function fn(param1, param2,) {}
// Cũng như
function fn(param1, param2) {}

(param1, param2,) => {};
// Cũng như
(param1, param2) => {};
```
Nó cũng hoạt động với method trong Class hoặc là trong Object
```javascript
class C {
  one(a,) {},
  two(a, b,) {},
}

var obj = {
  one(a,) {},
  two(a, b,) {},
};
```

## Gọi hàm
```javascript
fn(param1, param2,) {}
// Tương đương
fn(param1, param2) {}

// Chúng ta cũng có thể áp dụng với các global objects có sẵn trong Javascript:
Math.max(10, 20);
Math.max(10, 20,);
```

## Trường hợp lỗi

Có 2 trường hợp không được chấp nhận, đó là khi mà hàm không có param nào:

```javascript
function fn(,) {} // SyntaxError: missing formal parameter
(,) => {};       // SyntaxError: expected expression, got ','
fn(,)             // SyntaxError: expected expression, got ','
```

và sử dụng sau [rest parameter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters) :

```javascript
function f(...p,) {} // SyntaxError: parameter after rest parameter
(...p,) => {}        // SyntaxError: expected closing parenthesis, got ','
```

# Tổng kết
Trên là một vài tính năng nhỏ nhưng khá thú vị trong ES7 và ES8 tới, còn một vài tính năng nữa như là Async / Await, Shared Memory & Atomics,... nhưng mà nó không "nhỏ" nên mình không đưa vào, nhìn chung với một vài method mới trong Array, String với Object như trên thì cũng giúp cho ta đơn giản hóa kha khá thứ để làm.
