const fs = require('fs')
const slugify = require('slugify')
const argv = require('yargs').argv
const path = require('path')
const moment = require('moment')

const title = argv._.join(' ')
const slug = slugify(title, {
  replacement: '-',
  lower: true
})
const now = moment().format('YYYY-MM-DD')

const template = `---
title: ${title}
tags: 
category: 
excerpt: 
created: ${now}
image: ./images/${slug}.
image_caption: 
author: mnismt
---
`

fs.writeFileSync(path.join(__dirname, `content/blog/${slug}-${now}.md`), template)
